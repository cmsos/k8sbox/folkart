
variables:
  PACKAGES: "application"
  DEPENDENCY_LIST: ""
  IMAGE: "gitlab-registry.cern.ch/cmsos/buildah/cmsos-master-cs9-x64-full:2.2.0.0"
  RPM_REPO_DIR: "/tmp/$CI_PROJECT_NAME/$CI_JOB_NAME/repo"
  ARCHITECTURE: "x86_64"
  STORAGE_DRIVER: "vfs"
  BUILDAH_FORMAT: "docker"
  BUILDAH_IMAGE: "quay.io/buildah/stable:v1.21.0"

stages:
  - setup
  - rpm
  - buildah
  - helm
  - registry

prepare:
  stage: setup
  script:
    - env
    - mkdir -p $CI_PROJECT_DIR/prerequisites
    - touch  $CI_PROJECT_DIR/prerequisites/marktime #create marktime file as reference time
    - export MARK_TIME=$(date -r $CI_PROJECT_DIR/prerequisites/marktime '+%s')
    - echo $SHELL
    - stat $CI_PROJECT_DIR/prerequisites/marktime
    - date -r $CI_PROJECT_DIR/prerequisites/marktime '+%Y-%m-%d %H:%M:%S'
    - date -r $CI_PROJECT_DIR/prerequisites/marktime '+%s'
  artifacts:
    paths:
      - prerequisites/
  tags:
    - docker
  except:
    - pushes

build:
  stage: rpm
  image: $IMAGE
  dependencies:
    - prepare
  before_script:
    - export RPM_LIST="$PACKAGES"
  script:
    - gcc --version
    - export RPM_RELEASE_NAME=$CI_COMMIT_REF_NAME
    - export MARK_TIME=$(date -r $CI_PROJECT_DIR/prerequisites/marktime '+%s')
    - export RPM_BUILD_NUMBER="0."$MARK_TIME
    - export OPERATION_TYPE=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[1]}')
    - export RELEASE_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[2]}')
    - export BUILD_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[5]}')
    - if [ "$OPERATION_TYPE" == "release" -a "$BUILD_NUMBER" == "0" ]; then echo "Releases should use build number >= 1! Exiting..."; exit 1; fi
    - if [ "$OPERATION_TYPE" == "release" ]; then export RPM_RELEASE_NAME="r"$RELEASE_NUMBER; export RPM_BUILD_NUMBER=$BUILD_NUMBER; fi
    - if [ -v DEPENDENCY_LIST ]; then echo $DEPENDENCY_LIST; else export DEPENDENCY_LIST=""; echo $DEPENDENCY_LIST; fi
    - export PACKAGE_LIST="$DEPENDENCY_LIST $RPM_LIST"
    - echo $PACKAGE_LIST
    - echo "make rpm PACKAGES="$CI_JOB_NAME" PACKAGE_RELEASE="$RPM_RELEASE_NAME" BUILD_VERSION="$RPM_BUILD_NUMBER" BUILD_SUPPORT=build PROJECT_NAME="$CI_PROJECT_NAME
    - export XDAQ_ROOT=/opt/xdaq
    - echo "calling make with PACKAGES="$CI_JOB_NAME" PACKAGE_RELEASE="$RPM_RELEASE_NAME" BUILD_VERSION="$RPM_BUILD_NUMBER" BUILD_SUPPORT=build PROJECT_NAME="$CI_PROJECT_NAME
    - export SILENT=--silent
    - export QUIET=--quiet
    - make PACKAGES="$PACKAGE_LIST" BUILD_SUPPORT=build PROJECT_NAME=$CI_PROJECT_NAME MFDEFS_SUPPORT=1 PACKAGE_RELEASE=$RPM_RELEASE_NAME
    - make rpm PACKAGES="$RPM_LIST" PACKAGE_RELEASE=$RPM_RELEASE_NAME BUILD_VERSION=$RPM_BUILD_NUMBER BUILD_SUPPORT=build PROJECT_NAME=$CI_PROJECT_NAME
    - make installrpm PACKAGES="$RPM_LIST" BUILD_SUPPORT=build PROJECT_NAME=$CI_PROJECT_NAME
  artifacts:
    paths:
      - rpm/
  except:
    - pushes
  

docker:
  stage: buildah
  image: $BUILDAH_IMAGE
  dependencies:
    - build
    - prepare
  before_script:
    - buildah login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - env
    - export MAJOR_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[2]}')
    - export MINOR_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[3]}')
    - export PATCH_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[4]}')
    - export BUILD_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[5]}')
    - export OPERATION_TYPE=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[1]}')
    - VERSION_NUMBER=$MAJOR_NUMBER"."$MINOR_NUMBER"."$PATCH_NUMBER.$BUILD_NUMBER
    - echo "ARCHITECTURE="$ARCHITECTURE
    - echo "PROJECT="$PROJECT
    - export PACKAGE_RELEASE_NAME="+sha."$CI_COMMIT_SHORT_SHA
    - export MARK_TIME=$(date -r $CI_PROJECT_DIR/prerequisites/marktime '+%s')
    - if [ "$OPERATION_TYPE" != "release" ]; then VERSION_NUMBER="0.0.0.0-"$MARK_TIME; fi
    - IMAGE_TAG=$CI_REGISTRY_IMAGE"/cmsos-"$ARCHITECTURE"-"$CI_PROJECT_NAME":"$VERSION_NUMBER
    - echo "VERSION_NUMBER="$VERSION_NUMBER
    - echo "IMAGE_TAG="$IMAGE_TAG
    - mkdir -p info
    - echo $IMAGE_TAG > info/image_tag.txt
    - ls $CI_PROJECT_DIR/rpm/*.rpm
    - buildah bud -f Dockerfile -t $IMAGE_TAG $CI_PROJECT_DIR
    - buildah push $IMAGE_TAG
  artifacts:
    paths:
      - info/
  tags:
    - docker-privileged-xl
  except:
    - pushes
  

chart:
  stage: helm
  image: $IMAGE
  dependencies:
    - prepare
    - docker
    - build
  script:
    - echo "ARCHITECTURE="$ARCHITECTURE
    - echo "PROJECT="$PROJECT
    - helm version
    - export PACKAGE_RELEASE_NAME="+sha."$CI_COMMIT_SHORT_SHA
    - export MARK_TIME=$(date -r $CI_PROJECT_DIR/prerequisites/marktime '+%s')
    - export PACKAGE_BUILD_NUMBER=""
    - export OPERATION_TYPE=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[1]}')
    - export MAJOR_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[2]}')
    - export MINOR_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[3]}')
    - export PATCH_NUMBER=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[4]}')
    - if [ "$OPERATION_TYPE" != "release" ]; then MAJOR_NUMBER=0; MINOR_NUMBER=0; PATCH_NUMBER=0; PACKAGE_BUILD_NUMBER="-"$MARK_TIME; fi
    - export XDAQ_ROOT=/opt/xdaq
    - cat info/image_tag.txt
    - export DOCKER_IMAGE=`cat info/image_tag.txt`
    - echo $DOCKER_IMAGE
    - sed -r "s|^([[:space:]]*image[[:space:]]*:[[:space:]]).*|\1$DOCKER_IMAGE|" -i helm/values.yaml.extension
    - cat helm/values.yaml.extension
    - make -C helm
    - make -C helm package PROJECT_NAME=$CI_PROJECT_NAME PACKAGE_VER_MAJOR=$MAJOR_NUMBER PACKAGE_VER_MINOR=$MINOR_NUMBER PACKAGE_VER_PATCH=$PATCH_NUMBER PACKAGE_RELEASE=$PACKAGE_RELEASE_NAME BUILD_VERSION=$PACKAGE_BUILD_NUMBER
    - echo "done helm chart"
  artifacts:
    paths:
      - helm/chart/
  tags:
    - docker
  except:
    - pushes


cm push:
  stage: registry
  dependencies:
    - chart
  image:
    name: alpine/helm
    entrypoint: [""]
  script:
    - echo "CI_JOB_TOKEN:"$CI_JOB_TOKEN
    - echo "registry and pass:"${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}
    - echo "CI_API_V4_URL:"${CI_API_V4_URL}
    - export CHANNEL_NAME=devel
    - export OPERATION_TYPE=$(echo $CI_COMMIT_REF_NAME | awk '{split($0,a,"_"); print a[1]}')
    - export CHART_FILE_NAME=$(ls -tr helm/chart/*.tgz | tail -1| xargs -n 1 basename)
    - if [ "$OPERATION_TYPE" == "release" ]; then CHANNEL_NAME=stable; fi
    - echo "path="${CI_API_V4_URL}"/projects/"${CI_PROJECT_ID}"/packages/helm/"${CHANNEL_NAME}
    - echo "chartfilename="${CHART_FILE_NAME}
    - helm version
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm repo add --username ${CI_REGISTRY_USER} --password ${CI_JOB_TOKEN} ${CI_PROJECT_NAME} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/${CHANNEL_NAME}"
    - helm cm-push helm/chart/${CHART_FILE_NAME} ${CI_PROJECT_NAME}
  except:
    - pushes



