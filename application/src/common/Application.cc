/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P. Tzanis                        *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   		                 *
 *************************************************************************
 */

#include "application/Application.h"

XDAQ_INSTANTIATOR_IMPL(application::Application);

application::Application::Application(xdaq::ApplicationStub * s): xdaq::Application(s)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Hello Folks!");
}
