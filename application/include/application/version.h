/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P.Tzanis                         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */

#ifndef _application_version_h_
#define _application_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_APPLICATION_VERSION_MAJOR 1
#define CORE_APPLICATION_VERSION_MINOR 0
#define CORE_APPLICATION_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_APPLICATION_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_APPLICATION_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_APPLICATION_VERSION_CODE PACKAGE_VERSION_CODE(CORE_APPLICATION_VERSION_MAJOR,CORE_APPLICATION_VERSION_MINOR,CORE_APPLICATION_VERSION_PATCH)
#ifndef CORE_APPLICATION_PREVIOUS_VERSIONS
#define CORE_APPLICATION_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_APPLICATION_VERSION_MAJOR,CORE_APPLICATION_VERSION_MINOR,CORE_APPLICATION_VERSION_PATCH)
#else 
#define CORE_APPLICATION_FULL_VERSION_LIST  CORE_APPLICATION_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_APPLICATION_VERSION_MAJOR,CORE_APPLICATION_VERSION_MINOR,CORE_APPLICATION_VERSION_PATCH)
#endif 

namespace application
{
	const std::string project = "core";
	const std::string package  =  "application";
	const std::string versions = CORE_APPLICATION_FULL_VERSION_LIST;
	const std::string summary = "A simple XDAQ application";
	const std::string description = "Example of a simple XDAQ application to get started";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius, Tzanis Polyneikis";
	const std::string link = "https://gitlab.cern.ch/cmsos/k8sbox/folkart";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
