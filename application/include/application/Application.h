/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P. Tzanis                        *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   		                 *
 *************************************************************************
 */
#ifndef _application_Application_h_
#define _application_Application_h_

#include "xdaq/Application.h"

namespace application {

	class Application: public xdaq::Application
	{			
		public:

		XDAQ_INSTANTIATOR();
		
		Application(xdaq::ApplicationStub * s);

	};
}

#endif
